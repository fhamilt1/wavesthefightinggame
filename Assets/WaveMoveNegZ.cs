﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveMoveNegZ : MonoBehaviour {

	// Use this for initialization
	void Start () {
        StartCoroutine(DelayStart());
	}
    IEnumerator DelayStart() {
        yield return new WaitForSeconds(.5f);
        DriveMyCube.Instance.AddWaveZ(this.gameObject);
    }
	
	// Update is called once per frame
	void Update () {
        this.transform.Translate(Time.deltaTime * -.5f, 0, 0);
        if(this.transform.position.x > 4) {
            this.transform.position = new Vector3(4f, this.transform.position.y, this.transform.position.z);
        }
            
	}
}
