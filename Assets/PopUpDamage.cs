﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpDamage : MonoBehaviour {
    public Color startColor;
    public Color currentColor;
    public TextMesh myText;
	// Use this for initialization
	void Start () {
        currentColor.a = 0;
         
    }
	
	// Update is called once per frame
	void Update () {
        myText.color = currentColor;
        currentColor.a -= Time.deltaTime;

    }

    public void NewDamage(float dmg) {
        currentColor = startColor;
        myText.text = dmg.ToString();
    }
}
