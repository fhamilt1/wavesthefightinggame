﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class characterBase : MonoBehaviour {
    

    public float maxHPStat;
    public float currentHPStat;
    public float attackStat;
    public float defenseStat;
    public float speedStat;
    public int id;
    public float againstWavePercentage = .3f;
    public float withWavePercentage = 1.5f;
    public float baseStrafeSpeed = 1;
    public float baseFrontBackSpeed = 1;
    public float baseRotateSpeed = 10;
    public float radiusOfCharacter = .1f;
    private bool showradius = false;
    public GameObject showImpactPoint;

    public List<WaveSystem.WaveEffect> WaveEffects = new List<WaveSystem.WaveEffect>();
    public float waveMultiplier;
    public WaveSystem.WaveType myWaveType = WaveSystem.WaveType.WaveR;
    public LayerMask levelLayer;
    protected Vector3 MoveDirection;
    public bool DontMove = false;

    public virtual void Start() {

    }
    //public string name;


    /*
    public characterBase()
    {
		id = 0;
		name = "character";
		maxHPStat = 0.0f;
		currentHPStat = 0.0f;
		attackStat = 0.0f;
		defenseStat = 0.0f;
		speedStat = 0.0f;
    }
	*/


    public virtual void Update() {
        if (Input.GetKeyDown(KeyCode.P)) {
            showradius = !showradius;
        }
        if (showradius) ShowRadiusOfCharacter();


    }

    public float CalcWaveMultiplier(float x, float z) {
        float newWaveMultiplier = 1;
        foreach (WaveSystem.WaveEffect waveEffect in WaveEffects) {
            newWaveMultiplier = Mathf.Pow(newWaveMultiplier, 1) + Mathf.Pow(Vector3.Dot(this.transform.forward, waveEffect.direction) * waveEffect.strength, 1);
        }
        newWaveMultiplier = Mathf.Clamp(newWaveMultiplier, .1f, 2f);
        return newWaveMultiplier;
    }

    public Vector3 CalcMoveOffset(float x, float z, float waveMultiplier) {
        Vector3 moveOffset = Vector3.zero;
        moveOffset.x = baseStrafeSpeed * x * Time.deltaTime * waveMultiplier;
        moveOffset.z = z < 0 ? baseFrontBackSpeed * z * Time.deltaTime  * .4f : baseFrontBackSpeed * z * Time.deltaTime * waveMultiplier;
        //moveOffset.z = baseFrontBackSpeed * z * Time.deltaTime * waveMultiplier;
        moveOffset = moveOffset.x * this.transform.right + moveOffset.z * this.transform.forward;
        return moveOffset;
        //XMultiplier = XMultiplier + 1.15f * Mathf.Abs(Vector3.Dot(this.transform.forward, new Vector3(1, 0, 0))) * Vector3.Dot(this.transform.forward, new Vector3(1, 0, 0));

    }

    public float CalcRotateOffset(float delta, float waveMultiplier) {
        return delta * baseRotateSpeed * Time.deltaTime * waveMultiplier;
    }
    public void RotateCharacter(float angle) {
        if (DontMove) return;
        this.transform.Rotate(0, angle, 0);
    }

    private float DistanceMove(Vector3 moveOffset) {
        return Vector3.Distance(Vector3.zero, moveOffset);
    }
    public Vector3 lastHit = Vector3.zero;
    public Vector3 lastNormal = Vector3.zero;
    public Vector3 lastlast = Vector3.zero;
    public void MovePosition(Vector3 originalMoveOffset) {
        if (DontMove) return;
        Vector3 correctMovePosition = Vector3.zero;
        RaycastHit hitInfo;
        MoveDirection = this.transform.position + originalMoveOffset;
        if (Physics.SphereCast(this.transform.position , radiusOfCharacter, originalMoveOffset.normalized, out hitInfo, DistanceMove(originalMoveOffset),levelLayer )) {//this.transform.position, DistanceMove(originalMoveOffset) + radiusOfCharacter, 
            Vector3 newMovement = Vector3.Cross(hitInfo.normal, Vector3.up) * Vector3.Dot(originalMoveOffset, Vector3.Cross(hitInfo.normal, Vector3.up));
            OnHitWall(newMovement.normalized);
            
            MoveDirection = this.transform.position + newMovement;
                 
        }
        this.transform.position = MoveDirection;

    }

    public virtual void OnHitWall(Vector3 newMovement) {

    }

    public void ShowRadiusOfCharacter() {
        for (int i = -6; i < 6; i++) {
            Debug.DrawLine(this.transform.position + new Vector3(radiusOfCharacter * Mathf.Sin(i * Mathf.PI / 6), 0, radiusOfCharacter * Mathf.Cos(i * Mathf.PI / 6)), this.transform.position + new Vector3(radiusOfCharacter * Mathf.Sin((i + 1) * Mathf.PI / 6), 0, radiusOfCharacter * Mathf.Cos((i + 1) * Mathf.PI / 6)));

        }
    }


    public void OnCollisionStay(Collision collision) {
        WaveSystem.Wave wave = collision.collider.GetComponent<WaveSystem.Wave>();
        Debug.Log("THing");
        if (wave != null) {
            if (myWaveType == wave.myWaveType) {
                WaveSystem.WaveEffect waveEffect = new WaveSystem.WaveEffect();
                waveEffect.direction = wave.transform.forward;
                waveEffect.strength = wave.myStrength;
                WaveEffects.Add(waveEffect);
            }

        }
    }
    public void OnTriggerStay(Collider other) {
        WaveSystem.Wave wave = other.GetComponent<WaveSystem.Wave>();

        if (wave != null) {
            //Debug.Log(wave.name);
            if (myWaveType == wave.myWaveType) {
                WaveSystem.WaveEffect waveEffect = new WaveSystem.WaveEffect();
                waveEffect.direction = wave.transform.forward;
                waveEffect.strength = wave.myStrength;
                WaveEffects.Add(waveEffect);
            }

        }
    }
}
