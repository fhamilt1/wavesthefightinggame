﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasingEnemy : enemyCharacter {
    public Vector3 Momentum;
    public Vector3 TargetLocation;
    public float CourseCorrection;


    public Renderer WarningCylinder;
    public Renderer AttackCylinder;
    public float attackRange = .45f;
    public float attackTime = .3f;
    private float timeInWarningZone;
    protected float timeAttackCylinderVisible = .1f;

    // Use this for initialization
    public override void Start () {
        WarningCylinder.enabled = false;
        AttackCylinder.enabled = false;
	}
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
        
    }

    public void FixedUpdate() {
        Vector3 meToPlayer = playerCharacter.Instance.transform.position - this.transform.position;
        float distToPlayer = Vector3.Distance(meToPlayer, Vector3.zero);

        Chasing(distToPlayer, meToPlayer);
        Attacking(distToPlayer, meToPlayer);
        MovePosition(Momentum * Time.deltaTime * speedStat * .001f);
        this.transform.LookAt(playerCharacter.Instance.transform.position);

    }

    public virtual void Attacking(float distToPlayer, Vector3 vectorToPlayer) {
        if (distToPlayer < attackRange) {
            Momentum = Momentum * .3f;
            WarningCylinder.enabled = true;
            timeInWarningZone += Time.deltaTime;
            if (timeInWarningZone > attackTime) {
                timeInWarningZone = 0;
                StartCoroutine(ShowAttack());
            }
        }
        else {
            
           
            WarningCylinder.enabled = false;
            timeInWarningZone = 0;
        }
    }

    public void Chasing(float distToPlayer, Vector3 vectorToPlayer) {
        
        if (distToPlayer < VisualRange) {
            TargetLocation = playerCharacter.Instance.transform.position;
        }
        Vector3 steering = (vectorToPlayer.normalized - Momentum).normalized;
        Momentum = Vector3.Slerp(Momentum, (Momentum + steering).normalized, CourseCorrection).normalized;
    }
    public override void OnHitWall(Vector3 newMovement) {
        base.OnHitWall(newMovement);
        CorrectCourseCorrection(newMovement);
    }

    public IEnumerator ShowAttack() {
        AttackCylinder.enabled = true;
        yield return new WaitForSeconds(timeAttackCylinderVisible);
        AttackCylinder.enabled = false;
    }

    public void CorrectCourseCorrection(Vector3 usedCourse) {
        Momentum = Vector3.Slerp(Momentum, (usedCourse).normalized, CourseCorrection).normalized;
    }
}
