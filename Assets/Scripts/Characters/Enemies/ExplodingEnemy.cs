﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodingEnemy : ChasingEnemy {
    public float explodeDelay = .3f;
    public override void OnHitWall(Vector3 newMovement) {
        //base.OnHitWall(newMovement);
        StartCoroutine(Explode());
    }
    public override void Attacking(float distToPlayer, Vector3 vectorToPlayer) {
        if(distToPlayer< attackRange) {
            StartCoroutine( Explode());
        }
    }

    IEnumerator Explode() {
        yield return new WaitForSeconds(explodeDelay);
        StartCoroutine(ShowAttack());
        if (Vector3.Distance(this.transform.position, playerCharacter.Instance.transform.position)< attackRange) {
            //DoDamage
        }
        yield return new WaitForSeconds(timeAttackCylinderVisible);
        Destroy(this.gameObject);
    }

    public override void Start() {
        base.Start();
        this.Momentum = (playerCharacter.Instance.transform.position - this.transform.position).normalized;

    }
}
