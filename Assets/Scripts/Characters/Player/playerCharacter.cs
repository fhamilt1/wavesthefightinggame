﻿using GamepadInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class playerCharacter : characterBase {
    #region Singleton
    private static playerCharacter instance;

    private playerCharacter() { }

    public static playerCharacter Instance {
        get {
            if (instance == null) {
                instance = GameObject.FindObjectOfType<playerCharacter>();
            }
            return instance;
        }
    }
    #endregion
    //public List<GameObject> stances = new List<GameObject>();
    public Material swordMaterial;
	public Material spearMaterial;
	public float swordDamage;
	public float spearDamage;
	public Renderer rend;
	//private Animation attackAnim;
	public Animator myDudeAnimator;
	private string stance;
	public float damage;
    public Renderer myDude;
    public Collider weaponCollider;
    public Renderer weaponRend;
    public float waitTimeSword = .3f;
    public float waitTimeSpear = .3f;


	void Awake()
	{

        //swordAttack = GetComponent<Animation>();
        //spearAttack = GetComponent<Animation>();
        weaponRend.enabled = false;
        weaponCollider.enabled = false;
    }
	//rend = GetComponent<Renderer>();

	public void switchStanceSword()
	{
        myDudeAnimator.SetInteger("WeaponType", 0);
        myDude.material = swordMaterial;
		//attackAnim = swordAttack;
		stance = "sword";
		System.Console.WriteLine("sword");
		damage = swordDamage;
        myWaveType = WaveSystem.WaveType.WaveR;
    }
	public void switchStanceSpear()
	{
        myDudeAnimator.SetInteger("WeaponType", 1);
        myDude.material = spearMaterial;
		//attackAnim = spearAttack;
		stance = "spear";
		System.Console.WriteLine("spear");
		damage = spearDamage;
        myWaveType = WaveSystem.WaveType.WaveB;
    }

	public IEnumerator attack()
	{
        if(stance == "sword") {
            yield return new WaitForSeconds(waitTimeSword);
        }
        else {
            yield return new WaitForSeconds(waitTimeSpear);
        }
        //weaponRend.enabled = true;
        weaponCollider.enabled = true;
        yield return new WaitForSeconds(.2f);
        weaponRend.enabled = false;
        weaponCollider.enabled = false;
    }

	// Use this for initialization
	public override void Start () {
        base.Start();
        switchStanceSword();

    }
	
	// Update is called once per frame
	public override void Update () {
        base.Update();
       
		if (GamePad.GetButtonDown(GamePad.Button.B, GamePad.Index.One))
		{
			if(stance == "sword") {
                switchStanceSpear();
            }
            else {
                switchStanceSword();
            }
			
            
		}
		
		if (GamePad.GetButtonDown(GamePad.Button.A, GamePad.Index.One))
		{

            myDudeAnimator.SetTrigger("Attack");
            StartCoroutine(attack());

            //animator.SetInteger("state", 2);
		}
		
		
        waveMultiplier = CalcWaveMultiplier(GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.One).x, GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.One).y);
        MovePosition(CalcMoveOffset(GamePad.GetAxis(GamePad.Axis.LeftStick,GamePad.Index.One).x, GamePad.GetAxis(GamePad.Axis.LeftStick, GamePad.Index.One).y, waveMultiplier));
        RotateCharacter(CalcRotateOffset(GamePad.GetAxis(GamePad.Axis.RightStick, GamePad.Index.One).x,waveMultiplier));
        WaveEffects.Clear();
	}
}
