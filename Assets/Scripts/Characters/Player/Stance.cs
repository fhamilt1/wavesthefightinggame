﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stance : MonoBehaviour {

	public GameObject stanceObject;
	public string stanceName;
	public float stanceDamage;
	public Animation attack;
	public Material colorMat;
    
}
