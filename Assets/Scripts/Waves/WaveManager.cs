﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WaveSystem {


    public class WaveManager : MonoBehaviour {
        private static WaveManager instance;

        private WaveManager() { }

        public static WaveManager Instance {
            get {
                if (instance == null) {
                    instance = GameObject.FindObjectOfType<WaveManager>();
                }
                return instance;
            }
        }

        public List<WaveLauncher> WaveLaunchers = new List<WaveSystem.WaveLauncher>();
        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        public void AddWaveLauncher(WaveLauncher newWaveLauncher) {
            WaveLaunchers.Add(newWaveLauncher);
        }
    }



    

    public enum WaveType {
        WaveR,
        WaveG,
        WaveB
    }

    
}
