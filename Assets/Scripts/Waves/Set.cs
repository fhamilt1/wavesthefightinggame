﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace WaveSystem {
    public class Set : MonoBehaviour {
        public List<Wave> wavesPortions = new List<Wave>();
        public List<Wave> spawnedWaves = new List<Wave>();
        public float AngleOfInfluence = 70;
        public WaveType waveType;
        public float lifeSpan = 5;
        private float timeActive;
        public WaveLauncher myWaveLauncher;
        public float speed = 1;
        private float depth = 0;

        public void Start() {
           for(int i = 0; i < wavesPortions.Count; i++) {
                spawnedWaves.Add(Instantiate(wavesPortions[i]));
                spawnedWaves[i].transform.parent = this.transform;
                spawnedWaves[i].mySet = this;
                spawnedWaves[i].transform.localPosition = new Vector3(0, -.045f, -depth - spawnedWaves[i].transform.localScale.z / 2);
                depth += spawnedWaves[i].transform.localScale.z;
                spawnedWaves[i].transform.localRotation = Quaternion.identity;
            }
        }

        public void Update() {
            if (timeActive > lifeSpan) {
                Destroy(this.gameObject);
            }
            timeActive += Time.deltaTime;
            this.transform.Translate(0, 0, Time.deltaTime * speed);
            this.transform.localScale = new Vector3(Mathf.Tan(AngleOfInfluence*Mathf.PI/ 180) *2* Vector3.Distance(this.transform.position, myWaveLauncher.transform.position)*10, 1, 1);
        }
    }
}