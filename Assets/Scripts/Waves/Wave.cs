﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace WaveSystem {
    public class Wave : MonoBehaviour {
        public float myStrength;
        public Material myMaterial;
        public Set mySet;
        public WaveType myWaveType = WaveType.WaveR;
    }

    public class WaveEffect {
        public float strength;
        public Vector3 direction;
    }
}
