﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace WaveSystem {
    public class WaveLauncher : MonoBehaviour {
        public float SetFireRate = 1;
        private float _timeSinceFire;
        private int indexOfWaveSet = 0;
        public List<Set> waveSets = new List<Set>();
        public List<Set> waveSetsOut = new List<Set>();

        public void Start() {
            WaveManager.Instance.AddWaveLauncher(this);
            _timeSinceFire = SetFireRate;
        }

        public void Update() {
            if(_timeSinceFire > SetFireRate) {
                _timeSinceFire = 0;
                Set setOut = Instantiate(waveSets[indexOfWaveSet], this.transform.position, this.transform.rotation);
                waveSetsOut.Add(setOut);
                setOut.myWaveLauncher = this;
                indexOfWaveSet++;
                indexOfWaveSet = indexOfWaveSet >= waveSets.Count ? 0 : indexOfWaveSet;
            }
            _timeSinceFire += Time.deltaTime;
        }
    }
}