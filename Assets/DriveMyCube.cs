﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DriveMyCube : MonoBehaviour {
    private static DriveMyCube instance;

    private DriveMyCube() { }

    public static DriveMyCube Instance {
        get {
            if (instance == null) {
                instance = GameObject.FindObjectOfType<DriveMyCube>();
            }
            return instance;
        }
    }


    private List<GameObject> wavesZ;
    private List<GameObject> wavesNegZ;
    private List<GameObject> wavesX;
    private List<GameObject> wavesNegX;
    float XMultiplier;
    float ZMultiplier;
    float negZMultiplier;
    float xyMultiplier;

    // Use this for initialization
    void Awake () {
        wavesZ = new List<GameObject>();
        wavesNegZ = new List<GameObject>();
        wavesX = new List<GameObject>();
        wavesNegX = new List<GameObject>();
    }
	
	// Update is called once per frame
	void Update () {
        //Debug.Log(Input.GetAxis("Vertical"));
        ZMultiplier = 1.3f;
        XMultiplier = 1.3f;
        foreach (GameObject waveX in wavesX) {
            if(Mathf.Abs(this.transform.position.x - waveX.transform.position.x) < .5f) {
                XMultiplier = XMultiplier + 1.15f * Mathf.Abs(Vector3.Dot(this.transform.forward, new Vector3(1,0,0))) * Vector3.Dot(this.transform.forward, new Vector3(1, 0, 0));
            }
        }
        foreach (GameObject waveNegX in wavesNegX) {
            if (Mathf.Abs(this.transform.position.x - waveNegX.transform.position.x) < .5f) {
                XMultiplier = XMultiplier + 1.15f * Mathf.Abs(Vector3.Dot(this.transform.forward, new Vector3(-1, 0, 0))) * Vector3.Dot(this.transform.forward, new Vector3(-1, 0, 0));
            }
        }
        foreach (GameObject waveZ in wavesZ) {
            if (Mathf.Abs(this.transform.position.z - waveZ.transform.position.z) < .5f) {
                ZMultiplier = ZMultiplier + 1.15f * Mathf.Abs(Vector3.Dot(this.transform.forward, new Vector3(0, 0, 1))) * Vector3.Dot(this.transform.forward, new Vector3(0, 0, 1));
            }
        }
        foreach (GameObject waveNegZ in wavesNegZ) {
            if (Mathf.Abs(this.transform.position.z - waveNegZ.transform.position.z) < .5f) {
                ZMultiplier = ZMultiplier + 1.15f * Mathf.Abs(Vector3.Dot(this.transform.forward, new Vector3(0, 0, -1))) * Vector3.Dot(this.transform.forward, new Vector3(0, 0, -1));
            }
        }
        if(Input.GetAxis("Vertical") < 0) {
            XMultiplier = 1.3f;
            ZMultiplier = 1.3f;
            this.transform.Translate(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * .25f );
        }
        else {

            this.transform.Translate(0, 0, Input.GetAxis("Vertical") * Time.deltaTime * .5f * ZMultiplier * XMultiplier);
        }
        this.transform.Rotate(0, Input.GetAxis("Horizontal") * Time.deltaTime * 60 * Mathf.Lerp(.75f,1.5f,Mathf.InverseLerp(.15f,2.45f, XMultiplier)) * ZMultiplier, 0);
        if(this.transform.position.x > 2.5f) {
            this.transform.position = new Vector3(2.5f, this.transform.position.y, this.transform.position.z);
        }
        if (this.transform.position.x < -2.5f) {
            this.transform.position = new Vector3(-2.5f, this.transform.position.y, this.transform.position.z);
        }
        if (this.transform.position.z > 2.5f) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y,2.5f);
        }
        if (this.transform.position.z < -2.5f) {
            this.transform.position = new Vector3(this.transform.position.x, this.transform.position.y, -2.5f);
        }
    }

    public void AddWaveZ(GameObject newWave) {
        wavesZ.Add(newWave);
    }
    public void AddWaveNegZ(GameObject newWave) {
        wavesNegZ.Add(newWave);
    }
    public void AddWaveX(GameObject newWave) {
        wavesX.Add(newWave);
    }
    public void AddWaveNegX(GameObject newWave) {
        wavesNegX.Add(newWave);
    }
}
